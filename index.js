const axios = require('axios');
console.log(process.argv);
// total 2h50

// Output
// only a console.log at the end

// Presumption 
// we only fetch the html of the same website - same website = domain or subdomain
// we only show the links and image sources that are using HTTP
// we declare ourself to the website with a specific header
// we timeout the request after 60s
// The first input has to start with `http`
// the first input can point to any page, not just a domain

// Services
// URLCleaner = service that clean the URL - remove and tags, add '/' at the end of url, build relative paths
// Dispatcher = service that get URL to be called in a queue and dispatch them to fetchers
// Fetcher = fetcher/renderer of the URL - save the body in the storage
// Storage = Save bodies of pages, siteMap, and visited Webiste. It is the main storage for all services
// Handler = Handle the response of the fetcher. It register services that work in parallel on website source
// 
// services in parallel
// URLParser =  service that fetch the next URL, and add them to the the dispatcher if never visited
// MapParser = service that extract the links and images from the page

// we run 3 fetcher in parallel

// Distach to fetcher the URL
class Dispatcher {
  constructor(urlCleaner, output) {
    this.queue = []
    this.renderers = {}
    this.urlCleaner = urlCleaner
    this.output = output
  }

  add(url) {
    this.queue.push(url)
  }

  hasDispatchedAll() {
    return !this.queue.length && !Object.values(this.renderers).find(queue => queue.length > 0)
  }

  register(renderer) {
    this.renderers[renderer.id] = []

    this.assignUrl(renderer)
    this.triggerFetch(renderer)
  }

  getSleepingRenderer() {
    return Object.keys(this.renderers).find(key => !this.renderers[key].length)
  }

  assignUrl(renderer) {
    const url = this.queue.pop()
    if (url) {
      this.renderers[renderer.id].push(url)
    }
    // reveal these console.log to find the fetchers work in parallel
    // console.log("in queue of fetcher", this.renderers)
    // console.log("in queue of dispatcher", this.queue)
  }

  triggerFetch(renderer) {
    const url = this.renderers[renderer.id][0]

    if (!url) {
      if (this.hasDispatchedAll()) {
        return this.output.save()
      } else {
        return setTimeout(() => {
          this.assignUrl(renderer)
          this.triggerFetch(renderer)
        }, 100);
      }
    }

    renderer.fetch(url)
      .catch((error) => {
        // handle everything that is not 200 here
        if (error.response) {
          // if it is a response failing
          const { status = 500, headers } = error.response
          if ([301, 308].includes(status)) {
            return this.add(this.urlCleaner.concatenate(headers.location))
          }
        } else if (error.request) {
          // we should gather information, even retry
          const { method } = error.request
          console.log(`FAIL - ${method} ${url}`)
        } else {
          console.log(`FAIL - ${error.message}`)
          console.log(error)
        }
      })
      .finally(() => {
        // we remove the file from the queue
        this.renderers[renderer.id].pop()
        this.assignUrl(renderer)
        this.triggerFetch(renderer)
      })
  }
}

// Storage for site Map, pages, visited URLs
class HTMLStorage {
  constructor() {
    this.pages = {}
    this.visitedUrl = new Set()
    this.siteMap = {}
  }

  add(url, content) {
    this.pages[url] = content
  }

  fetch(url) {
    return this.pages[url]
  }
}

// Fecther - call the URL and get back the HTML
// A fetcher should more complex than just doing a GET request
// It should get HTML and JS, and render the page properly.
class Fetcher {
  constructor(storage, handler, dispatcher) {
    this.id = parseInt(Math.floor(Math.random() * 100000))
    this.storage = storage
    this.handler = handler

    dispatcher.register(this)
  }

  fetch(url) {
    return axios.get(url, {
      maxRedirects: 0,
      headers: {
        // we handle only text/html 
        "Accept": "text/html",
        "User-Agent": "SiteMaper/1.0"
      },
      timeout: 60000
    })
      .then(response => {
        const HTMLPage = response.data
        this.storage.add(url, HTMLPage)
        this.handler.handle(url)
      })
  }
}

// Handle the files and send it to all services require for parallele analyses
class Handler {
  constructor(storage) {
    // this.queue = []
    this.services = []
    this.storage = storage
  }

  register(service) {
    this.services.push(service)
  }

  handle(url) {
    this.services.forEach(service => service.handle(url, this.storage.pages[url]))
  }
}

class Service {
  constructor(handler) {
    handler.register(this)
  }
}

class URLParser extends Service {
  constructor(storage, handler, dispatcher, urlCleaner) {
    super(handler)
    this.storage = storage
    this.dispatcher = dispatcher
    this.urlCleaner = urlCleaner
  }

  handle(url, file) {
    // use regex to return the next URL then call the dispatcher
    const linkMatches = file.matchAll(/<a[^>]*href="([^"]*)"[^>]*>/gm)
    const links = [...linkMatches]
      .map(match => match[1])
      .map(link => this.urlCleaner.clean(link))
      .filter(link => this.urlCleaner.belongsToDomain(link))

    links.forEach((link) => {
      if (!this.storage.visitedUrl.has(link)) {
        this.storage.visitedUrl.add(link)
        this.dispatcher.add(link)
      }
    })
  }
}

class MapParser extends Service {
  constructor(storage, handler, urlCleaner) {
    super(handler)
    this.storage = storage
    this.urlCleaner = urlCleaner
  }

  handle(url, file) {
    // use regex to get all URLs and Images and store them in the response
    const linkMatches = file.matchAll(/<a[^>]*href="([^"]*)"[^>]*>/gm)
    // we spread the match iterator and map over the matches to get each match (in position 1)
    const links = [...linkMatches]
      .map(match => match[1])
      .map(link => this.urlCleaner.clean(link))
      .filter(link => !!link)

    const imgMatches = file.matchAll(/<img[^>]*src="([^"]*)"[^>]*>/gm)
    const images = [...imgMatches]
      .map(match => match[1])
      .map(img => this.urlCleaner.clean(img))
      .filter(link => !!link)

    this.storage.siteMap[this.urlCleaner.clean(url)] = { links: [... new Set(links)], images: [...new Set(images)] }
    console.log("Site Map contains the URL of", this.urlCleaner.clean(url))
  }
}

// Clean and standardize the URLs
// we took some assumptions in our cases:
//
// 1- we filter URL which precise they are on http protocol only,
// knowing some URL could directly start with www.
// We could add HTTP before the URL, while being aware of other protocols and data:svg
//
// 2- We accept relative paths + query parameters, we remove the tags (everything after by '#')
//
class URLCleaner {
  constructor(domain) {
    this.domain = this.constructor.standardize(this.constructor.extractDomain(domain))
    if (!this.domain) {
      throw new Error(`${domain} is not a valid URL provided`)
    }
  }

  static extractDomain(url) {
    return url.match(/^(http[s]?:\/\/[^\/]*)/)[1] || ''
  }

  static standardize(url) {
    // we ignore tags, we decided to accept queryparameters
    let finalUrl = url.match(/([^#]*).*/)[1]

    // we always add the '/' at the end
    if (finalUrl.length > 0 && finalUrl[finalUrl.length - 1] !== '/') {
      finalUrl = finalUrl + '/'
    }

    return finalUrl
  }

  belongsToDomain(path) {
    // Returns null when do not belong to the domain
    const regex = new RegExp(this.domain)
    if (!path.match(regex)) {
      return false
    }

    return true
  }

  concatenate(path) {
    if (path[0] === '/') {
      return this.domain + path.slice(1)
    }

    return this.domain + path
  }

  clean(path) {
    // we concatenate if the path is relative
    let finalUrl = ''
    if (path[0] === '/') {
      finalUrl = this.concatenate(path)
    } else {
      finalUrl = path
    }

    // we ignore the URL that are not http
    if (!finalUrl.match(/^http/)) {
      return ''
    }

    return this.constructor.standardize(finalUrl)
  }
}

// Just return siteMap when all is over
class Output {
  constructor(storage) {
    this.storage = storage
    this.isAlreadySaved = false
  }

  save() {
    if (!this.isAlreadySaved) {
      this.isAlreadySaved = true
      console.log(this.storage.siteMap)
    }
  }
}

// the first 2 parameters are variables we can ignore
const target = process.argv[2]

if (!target) {
  throw new Error('you need to provide a website to get started')
}

if (!target.match(/^http/)) {
  throw new Error('The URL has to be a HTTP or HTTPS URL')
}

const url = URLCleaner.standardize(target)
const urlCleaner = new URLCleaner(url)
const storage = new HTMLStorage()
const output = new Output(storage)
const dispatcher = new Dispatcher(urlCleaner, output)
dispatcher.add(url)
const handler = new Handler(storage)

// services
new URLParser(storage, handler, dispatcher, urlCleaner)
new MapParser(storage, handler, urlCleaner)

// fetchers
new Fetcher(storage, handler, dispatcher)
new Fetcher(storage, handler, dispatcher)
new Fetcher(storage, handler, dispatcher)
